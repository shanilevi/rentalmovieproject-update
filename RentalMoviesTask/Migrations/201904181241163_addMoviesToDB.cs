namespace RentalMoviesTask.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMoviesToDB : DbMigration
    {
        public override void Up()
        {
            
            Sql("INSERT INTO Movies (Name,GenreId,NumberInStock,ReleasedDate,DateAdded) Values ('Wonderwoman',3,4,'2017-01-01','2017-02-01')");
            Sql("INSERT INTO Movies (Name,GenreId,NumberInStock,ReleasedDate,DateAdded) Values ('Superman',3,5,'2013-01-01','2013-02-01')");
            Sql("INSERT INTO Movies (Name,GenreId,NumberInStock,ReleasedDate,DateAdded) Values ('Titanic',2,6,'2003-01-01','2003-02-01')");
            Sql("INSERT INTO Movies (Name,GenreId,NumberInStock,ReleasedDate,DateAdded) Values ('Aquaman',3,2,'2019-01-01','2019-02-01')");

        }

        public override void Down()
        {
        }
    }
}
