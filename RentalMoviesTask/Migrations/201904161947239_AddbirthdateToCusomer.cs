namespace RentalMoviesTask.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddbirthdateToCusomer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "birthDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Customers", "birthDate");
        }
    }
}
