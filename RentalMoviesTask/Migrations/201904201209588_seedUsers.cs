namespace RentalMoviesTask.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'01783d74-112d-409a-9917-40c465330e6b', N'guest@gmail.com', 0, N'AFFV2w7v0yLQ2Ic/y9LU8Y03vI35zdZ2A1zNqBDzQ4tBp6iIjYfIq30k3VdVlkdoiA==', N'62d5d36f-18f6-410d-8a95-2aaa3d4be251', NULL, 0, 0, NULL, 1, 0, N'guest@gmail.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'8aa94fc8-cbea-4eb9-9c16-77fe8a815dbc', N'shani.levi.28.6@gmail.com', 0, N'APAybm9ROvqhH67I/aiqPz6hNGe8+YYz6PTi6ZwOEzh2WWJmfyoKKyqBRvSpqB2B4w==', N'1971c78d-02fc-4a17-b1b7-f688f5096dac', NULL, 0, 0, NULL, 1, 0, N'shani.levi.28.6@gmail.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'fd3edfeb-519d-4a25-9d18-d6c038e5373c', N'Admin@gmail.com', 0, N'ALLZIRdCWJGUOX25lt0z8PUqAH6l0qRrwexnFKNpyYvIYPsNFgYtiWdDWfDIzFIyXw==', N'9a372670-9c31-4514-8ce8-da808bd8ae6b', NULL, 0, 0, NULL, 1, 0, N'Admin@gmail.com')

INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'458add1e-e0fc-4cb2-b39f-df85b52f22ef', N'Admin')

INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'fd3edfeb-519d-4a25-9d18-d6c038e5373c', N'458add1e-e0fc-4cb2-b39f-df85b52f22ef')

");
        }
        
        public override void Down()
        {
        }
    }
}
