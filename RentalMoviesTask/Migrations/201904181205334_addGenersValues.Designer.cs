// <auto-generated />
namespace RentalMoviesTask.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class addGenersValues : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addGenersValues));
        
        string IMigrationMetadata.Id
        {
            get { return "201904181205334_addGenersValues"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
