﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RentalMoviesTask.Models
{
    public class Rental
    {

        public int Id { get; set; } 
        public String Customer { get; set; }

        public int Movie { get; set; }

    }
}