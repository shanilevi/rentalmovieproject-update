﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace RentalMoviesTask.Models
{
    public class Customer
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="Please enter customer's name")]
        [StringLength(255)]
         public string Name { get; set; }

        public DateTime? birthDate { get; set; }

        public bool IsSubscribedToNewsLetter { get; set; }

        public MembershipTypes MembershipType { get; set;}

        [Display(Name="Membership Type")]
        public byte MembershipTypeId { get; set; }
    }
}