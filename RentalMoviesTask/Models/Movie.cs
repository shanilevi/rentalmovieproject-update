﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace RentalMoviesTask.Models
{
    public class Movie
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public Genre Genre { get; set; }

        [Display(Name = "Genre")]
        [Required]
        public byte GenreId { get; set; }

        [Display(Name = "Number in stock")]
        [Required]
        [Range(1,20)]
        public int NumberInStock { get; set; }

        [Display(Name ="Released Date")]
        public DateTime ReleasedDate { get; set; }

        public DateTime DateAdded { get; set; }

        public byte NumberAvailable { get; set; }

    }

    
}