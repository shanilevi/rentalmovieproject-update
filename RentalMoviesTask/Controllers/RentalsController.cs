﻿using Microsoft.AspNet.Identity;
using RentalMoviesTask.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RentalMoviesTask.Controllers
{
    public class RentalsController : Controller
    {
        private ApplicationDbContext _context;

        public RentalsController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: Rentals
        public ActionResult Index()
        {
            
            var user = System.Web.HttpContext.Current.User.Identity.GetUserId();
            var rentals = _context.Rentals.Where(
                    r => r.Customer.Contains(user)).ToList();

            List<Movie> movies = new List<Movie>();

            foreach (var rental in rentals) {
                var movieInDB = _context.Movies.Single(m => m.Id == rental.Movie);
                movies.Add(movieInDB);         
            }

            return View(movies);
        }
    }
}