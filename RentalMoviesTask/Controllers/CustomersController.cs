﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RentalMoviesTask.Models;
using System.Data.Entity;
using RentalMoviesTask.ViewModels;

namespace RentalMoviesTask.Controllers
{
    public class CustomersController : Controller
    {
        private ApplicationDbContext _context;

        public CustomersController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: Customer
        public ViewResult Index()
        {
            var customers = _context.customers.Include(c => c.MembershipType).ToList();
            if(User.IsInRole("Admin"))
                return View("IndexAdmin",customers);

            return View("IndexUser", customers);

        }
        [Authorize(Roles ="Admin")]
        public ActionResult New(){

            var membershipTypes = _context.MembershipTypes.ToList();
            var viewModel = new CustomerFormViewModel()
            {
                Customer=new Customer(),
                MembershipTypes = membershipTypes
            };
            return View("CustomerForm", viewModel);   
        }
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Customer customer)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new CustomerFormViewModel
                {
                    Customer = customer,
                    MembershipTypes = _context.MembershipTypes.ToList()
                };
            return View("CustomerForm", viewModel);
            }
            if (customer.Id == 0)
                _context.customers.Add(customer);       
            else
            {
                var customerInDB = _context.customers.Single(c => customer.Id == c.Id);

                customerInDB.Name = customer.Name;
                customerInDB.IsSubscribedToNewsLetter = customer.IsSubscribedToNewsLetter;
                customerInDB.MembershipTypeId = customer.MembershipTypeId;
                customerInDB.birthDate = customer.birthDate;
            }
            _context.SaveChanges();

            return RedirectToAction("Index", "customers");
        }
        public ActionResult Details(int id)
        {
            var customer = _context.customers.Include(c => c.MembershipType).SingleOrDefault(c => c.Id == id);

            if (customer == null)
                return HttpNotFound();

            return View(customer);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int id)
        {
            var customer = _context.customers.SingleOrDefault(c => c.Id == id);
            //if customer exsit in DB
            if (customer==null)
            {
                return HttpNotFound();
            }
            var viewModel = new CustomerFormViewModel()
            {
                Customer = customer,
                MembershipTypes = _context.MembershipTypes.ToList()

            };
            return View("CustomerForm", viewModel);
        }

    }
}