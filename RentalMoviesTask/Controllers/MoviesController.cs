﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RentalMoviesTask.Models;
using System.Data.Entity;
using RentalMoviesTask.ViewModels;
using Microsoft.AspNet.Identity;

namespace RentalMoviesTask.Controllers
{

    public class MoviesController : Controller
    {
        private ApplicationDbContext _context;

        public MoviesController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: Movies/
        public ViewResult Index()
        {
            var movies = _context.Movies.Include(m => m.Genre).ToList();

            if (User.IsInRole("Admin"))
            {
                return View("IndexAdmin", movies);
            }
            return View("IndexUser", movies);

        }

        public ActionResult createNewRentals(int id)
        {

            String user = System.Web.HttpContext.Current.User.Identity.GetUserId();

            var rental = new Rental
            {
                Customer = user,
                Movie = id
            };

            _context.Rentals.Add(rental);       
            _context.SaveChanges();

            return RedirectToAction("Index", "Rentals");
        }


        [Authorize(Roles ="Admin")]
        public ActionResult New()
        {
            var genres = _context.Genres.ToList();
           
            var viewModel = new FormMovieViewModel()
            {
                
                Genres = genres
            };

            return View("MovieForm",viewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Save(Movie movie)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new FormMovieViewModel(movie)
                {
                    
                    Genres = _context.Genres.ToList()
                };
                return View("MovieForm", viewModel);
            }
            if(movie.Id==0)
            {
                movie.DateAdded = DateTime.Now;
                _context.Movies.Add(movie);
            }
            else
            {
                var movieInDB = _context.Movies.Single(m => m.Id == movie.Id);
                movieInDB.Name = movie.Name;
                movieInDB.GenreId = movie.GenreId;
                movieInDB.ReleasedDate = movie.ReleasedDate;
                movieInDB.NumberInStock = movie.NumberInStock;
            }
            _context.SaveChanges();
            return RedirectToAction("Index", "Movies");
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int id)
        {
            var movie = _context.Movies.SingleOrDefault(m => m.Id == id);
            
            if (movie == null)
                return HttpNotFound();

            var viewModel = new FormMovieViewModel(movie)
            {
             
                Genres = _context.Genres.ToList()
            };

            return View("MovieForm",viewModel);
        }
        
        public ActionResult Details(int id)
        {
            var movie = _context.Movies.Include(m => m.Genre).SingleOrDefault(m => m.Id == id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        private IEnumerable<Movie> GetMovies()
        {
            return new List<Movie>
            {
                new Movie {Id=1,Name="Shrek 1"},
                new Movie { Id=2, Name="Wonder Women"}
            };
        }
        //example of attribute routing
        // GET: Movies/released/{year}/{month}
        [Route("movies/released/{year:regex(\\d{4})}/{month:regex(\\d{2})}")]
        public ActionResult ByReleasedDate(int year,int month)
        {
            return Content(year + "/" + month);
        }
    }
}