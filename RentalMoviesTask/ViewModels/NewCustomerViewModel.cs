﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RentalMoviesTask.Models;

namespace RentalMoviesTask.ViewModels
{
    public class CustomerFormViewModel
    {
        public IEnumerable<MembershipTypes> MembershipTypes { get; set; }
        public Customer Customer { get; set; }
        public String Title
        {
            get
            {
                return (Customer.Id != 0) ? "Edit Customer" : "New Customer";
            }
        }
    }
}